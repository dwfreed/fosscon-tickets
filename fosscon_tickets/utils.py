from . import app
import sqlalchemy.types
import json
import inflect
import decimal
import flask

def log_exception(exc_info=1):
	args = {'method': flask.request.method,
		'path': flask.request.path}
	app.logger.error("Exception on %(path)s [%(method)s]\n", args, exc_info=exc_info)

def calc_stripe_fee(amount):
	# total_charge = (1000 * amount + 30000) / 971
	dec_amount = decimal.Decimal(amount)
	total_charge = dec_amount.fma(1000, 30000) / 971
	return int(total_charge.to_integral_value()) - amount

@app.template_filter()
def format_price(amount):
	return "$%d.%02d" % divmod(amount, 100)

inflect_engine = inflect.engine()

@app.template_filter()
def to_ordinal(number):
	return inflect_engine.number_to_words(inflect_engine.ordinal(number))

class JSONEncodedDict(sqlalchemy.types.TypeDecorator):
	impl = sqlalchemy.types.VARCHAR

	def process_bind_param(self, value, dialect):
		if value is not None:
			value = json.dumps(value)
		return value

	def process_result_value(self, value, dialect):
		if value is not None:
			value = json.loads(value)
		return value
