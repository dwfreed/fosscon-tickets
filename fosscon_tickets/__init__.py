import flask
from flask.ext.sqlalchemy import SQLAlchemy
import stripe
import logging

app = flask.Flask(__name__, static_url_path='')
app.config.from_pyfile('../config.py')

app.logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('logs/app.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%F %T')
handler.setFormatter(formatter)
app.logger.addHandler(handler)

stripe.api_key = app.config['STRIPE_SECRET_KEY']

db = SQLAlchemy(app)

from . import models
from . import controllers
from . import utils

db.create_all()
