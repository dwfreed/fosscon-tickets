import flask.ext.wtf as flask_wtf
import wtforms.fields as fields
import wtforms.fields.html5 as html5fields
import wtforms.validators as validators
import wtforms.widgets as widgets

class Ticket(flask_wtf.Form):
	ticket_id = html5fields.IntegerField('ID', [validators.InputRequired()], widget=widgets.HiddenInput())
	quantity = html5fields.IntegerField('Quantity', [validators.InputRequired(), validators.NumberRange(0, 100)], default=0)

class Tickets(flask_wtf.Form):
	tickets = fields.FieldList(fields.FormField(Ticket))

class Detail(flask_wtf.Form):
	ticket_id = html5fields.IntegerField('ID', [validators.InputRequired()], widget=widgets.HiddenInput())
	name = fields.StringField('Name', [validators.InputRequired()])
	contact_email = fields.StringField('Email Address', [validators.InputRequired(), validators.Email()])
	company = fields.StringField('Company')
	website = fields.StringField('Website')
	twitter = fields.StringField('Twitter')
	callsign = fields.StringField('Callsign')
	shirt_size = fields.SelectField('Shirt Size', choices=[('small', 'Small'), ('medium', 'Medium'), ('large', 'Large'), ('xl', 'XL'), ('xxl', 'XXL'), ('wsmall', 'Women\'s Small'), ('wmedium', 'Women\'s Medium'), ('wlarge', 'Women\'s Large'), ('wxl', 'Women\'s XL'), ('wxxl', 'Women\'s XXL')])
	amateur_exam = fields.SelectMultipleField('Ham Radio Exam', choices=[('none', 'No thanks'), ('tech', 'Technician'), ('gen', 'General'), ('extra', 'Extra')])
	hackfest = fields.BooleanField('I\'d like to participate in the hackfest')
	email_me = fields.BooleanField('Subscribe to fosscon announcement mailing list')

class Details(flask_wtf.Form):
	details = fields.FieldList(fields.FormField(Detail))
	recaptcha = flask_wtf.RecaptchaField('Recaptcha')
	stripeToken = fields.StringField('Stripe Token')
	stripeEmail = fields.StringField('Stripe Email')

	def __init__(self, paid, *args, **kwargs):
		super(Details, self).__init__(*args, **kwargs)
		if paid:
			self.recaptcha.validators = []
			self.stripeToken.validators = [validators.InputRequired()]
			self.stripeEmail.validators = [validators.InputRequired(), validators.Email()]

class Guest(flask_wtf.Form):
	guest_id = html5fields.IntegerField('ID', [validators.InputRequired()], widget=widgets.HiddenInput())
	checked_in = fields.BooleanField('Checked in')
	edited = fields.BooleanField('Changed record')
	name = fields.StringField('Name', [validators.InputRequired()])
	contact_email = fields.StringField('Email Address', [validators.InputRequired(), validators.Email()])
	company = fields.StringField('Company')
	website = fields.StringField('Website')
	twitter = fields.StringField('Twitter')
	callsign = fields.StringField('Callsign')
	shirt_size = fields.SelectField('Shirt Size', choices=[('small', 'Small'), ('medium', 'Medium'), ('large', 'Large'), ('xl', 'XL'), ('xxl', 'XXL'), ('wsmall', 'Women\'s Small'), ('wmedium', 'Women\'s Medium'), ('wlarge', 'Women\'s Large'), ('wxl', 'Women\'s XL'), ('wxxl', 'Women\'s XXL')])
	amateur_exam = fields.SelectMultipleField('Ham Radio Exam', choices=[('none', 'No thanks'), ('tech', 'Technician'), ('gen', 'General'), ('extra', 'Extra')])
	hackfest = fields.BooleanField('I\'d like to participate in the hackfest')
	email_me = fields.BooleanField('Subscribe to fosscon announcement mailing list')

class Guests(flask_wtf.Form):
	guests = fields.FieldList(fields.FormField(Guest))
