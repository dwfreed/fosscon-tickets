from . import app, db, models, forms, utils
import flask
import wtforms.fields as fields
import pickle
import stripe

@app.errorhandler(500)
def internal_server_error(exception):
	flask.flash('An unknown error occurred. Please go back and try again.')
	flask.flash('If you continue to receive this error, please contact us at help@fosscon.us')
	return flask.render_template('internal_server_error.html'), 500

@app.route('/')
def index():
	if not flask.get_flashed_messages():
		flask.session.clear()
	tickets = models.Ticket.query.all()
	tickets_len = len(tickets)
	forms.Tickets.tickets = fields.FieldList(fields.FormField(forms.Ticket), min_entries=tickets_len, max_entries=tickets_len)
	form = forms.Tickets()
	comboTicketsModelForm = zip(tickets, form.tickets)
	for modelTicket, formTicket in comboTicketsModelForm:
		formTicket.form.ticket_id.data = modelTicket.id
	return flask.render_template('index.html', form=form, comboTicketsModelForm=comboTicketsModelForm)

@app.route('/', methods=['POST'])
def index_submit():
	form = forms.Tickets()
	if form.validate_on_submit():
		flask.session['tickets'] = {}
		quantity = 0
		for ticket in form.tickets:
			flask.session['tickets'][ticket.ticket_id.data] = ticket.quantity.data
			quantity += ticket.quantity.data
		if not quantity:
			flask.flash('You must select at least one ticket!')
			return flask.redirect(flask.url_for('index'))
		return flask.redirect(flask.url_for('details'))
	else:
		csrf_failed = False
		id_failed = False
		quantity_failed = False
		for field, errors in form.errors.items():
			if not csrf_failed and field == 'csrf_token':
				csrf_failed = True
				flask.flash('CSRF token missing! Stop hacking mah forms!')
			elif field != 'csrf_token':
				for error in errors:
					for field, errors in error.items():
						if not csrf_failed and field == 'csrf_token':
							csrf_failed = True
							flask.flash('CSRF token missing! Stop hacking mah forms!')
						elif not id_failed and field == 'ticket_id':
							id_failed = True
							flask.flash('ID missing! Stop hacking mah forms!')
						elif not quantity_failed and field == 'quantity':
							quantity_failed = True
							flask.flash('Quantity is required! Set it to 0 if you don\'t want any of that ticket')
						elif field not in ('csrf_token', 'ticket_id', 'quantity'):
							flask.flash('%s: %s' % (field, ', '.join(errors)))
		return flask.redirect(flask.url_for('index'))

@app.route('/details')
def details():
	tickets = models.Ticket.query.all()
	tickets_dict = {ticket.id: ticket for ticket in tickets}
	guests_ticket = []
	indexes = []
	subtotal = 0
	for ticket_id, count in flask.session['tickets'].items():
		guests_ticket += [tickets_dict[int(ticket_id)]] * count
		indexes += range(1, count + 1)
		subtotal += tickets_dict[int(ticket_id)].cost * count
	if subtotal:
		fee = utils.calc_stripe_fee(subtotal)
	else:
		fee = 0
	flask.session['money'] = bool(subtotal)
	total_tickets = len(guests_ticket)
	forms.Details.details = fields.FieldList(fields.FormField(forms.Detail), min_entries=total_tickets, max_entries=total_tickets)
	if 'formdata' in flask.session:
		form = forms.Details(False, formdata=pickle.loads(flask.session['formdata']))
	else:
		form = forms.Details(False)
	comboTicketDetail = zip(indexes, guests_ticket, form.details)
	for index, ticket, detail in comboTicketDetail:
		detail.form.ticket_id.data = ticket.id
	return flask.render_template('details.html', form=form, comboTicketDetail=comboTicketDetail, subtotal=subtotal, fee=fee, stripe_key=app.config['STRIPE_PUBLISHABLE_KEY'])

@app.route('/details', methods=['POST'])
def details_submit():
	flask.session['formdata'] = pickle.dumps(flask.request.form)
	form = forms.Details(flask.session['money'])
	if form.validate_on_submit():
		tickets = models.Ticket.query.all()
		tickets_dict = {ticket.id: ticket for ticket in tickets}
		stripeCustomer = None
		subtotal = 0
		flask.session['tickets'] = {}
		guests = []
		for detailFormField in form.details:
			detailForm = detailFormField.form
			ticket = tickets_dict[detailForm.ticket_id.data]
			if not ticket.id in flask.session['tickets']:
				flask.session['tickets'][ticket.id] = 0
			flask.session['tickets'][ticket.id] += 1
			subtotal += ticket.cost
			options = {}
			options['amateur_exam'] = detailForm.amateur_exam.data
			options['hackfest'] = detailForm.hackfest.data
			options['email_me'] = detailForm.email_me.data
			if ticket.kind == 'paid':
				options['shirt_size'] = detailForm.shirt_size.data
			guest = models.Guest(detailForm.name.data, detailForm.contact_email.data, detailForm.company.data, detailForm.website.data, detailForm.twitter.data, detailForm.callsign.data, ticket.id, options)
			guests.append(guest)
			db.session.add(guest)
		if subtotal:
			fee = utils.calc_stripe_fee(subtotal)
			flask.session['money'] = {'subtotal': subtotal, 'fee': fee}
			stripeToken = form.stripeToken.data
			stripeEmail = form.stripeEmail.data
			try:
				stripeCustomer = stripe.Customer.create(card=stripeToken, email=stripeEmail)
				for guest in guests:
					guest.stripeCustomer = stripeCustomer.id
					db.session.add(guest)
				for ticket_id, count in flask.session['tickets'].items():
					if count > 0:
						description = "%d %s" % (count, utils.inflect_engine.plural(tickets_dict[int(ticket_id)].description, count))
						amount = tickets_dict[int(ticket_id)].cost * count
						stripe.InvoiceItem.create(customer=stripeCustomer.id, amount=amount, currency='usd', description=description)
				stripe.InvoiceItem.create(customer=stripeCustomer.id, amount=fee, currency='usd', description='Stripe fee')
				invoice = stripe.Invoice.create(customer=stripeCustomer.id)
				invoice.pay()
			except stripe.error.CardError as e:
				utils.log_exception()
				if e.code == 'incorrect_number':
					message = 'The card number you entered is incorrect. Please try again.'
				elif e.code == 'invalid_number':
					message = 'The card number you entered is invalid. Please try again.'
				elif e.code in ('invalid_expiry_month', 'invalid_expiry_year'):
					message = 'The expiration date you entered is invalid. Please try again.'
				elif e.code == 'invalid_cvc':
					message = 'The CVC code you entered is invalid. Please try again.'
				elif e.code == 'expired_card':
					message = 'Your card is expired. Please try another card.'
				elif e.code == 'incorrect_cvc':
					message = 'The CVC code you entered is incorrect. Please try again.'
				elif e.code == 'incorrect_zip':
					message = 'The ZIP code you entered is incorrect. Please try again.'
				elif e.code == 'card_declined':
					message = 'Your card was declined. Please try another card.'
				elif e.code == 'missing':
					message = 'A strange error occurred. Please try again in a few minutes.'
				elif e.code == 'processing_error':
					message = 'Stripe encountered an error during processing. Please try again in a few minutes.'
				elif e.code == 'rate_limit':
					message = 'A rate limit has been reached. Please try again in a few minutes.'
				flask.flash(message)
				flask.flash('If you continue to receive this error, please contact us at help@fosscon.us')
				db.session.rollback()
				return flask.redirect(flask.url_for('details'))
			except stripe.error.StripeError as e:
				utils.log_exception()
				flask.flash('An unknown Stripe error occurred. Please try again in a few minutes.')
				flask.flash('If you continue to receive this error, please contact us at help@fosscon.us')
				db.session.rollback()
				return flask.redirect(flask.url_for('details'))
		db.session.commit()
		del flask.session['formdata']
		flask.session['guests'] = [guest.id for guest in guests]
		return flask.redirect(flask.url_for('confirmation'))
	else:
		csrf_failed = False
		id_failed = False
		name_failed = False
		email_failed = False
		for field, errors in form.errors.items():
			if not csrf_failed and field == 'csrf_token':
				csrf_failed = True
				flask.flash('CSRF token missing! Stop hacking mah forms!')
			elif not flask.session['money'] and field == 'recaptcha':
				flask.flash('Recaptcha validation failed!')
			elif flask.session['money'] and field == 'stripeToken':
				flask.flash('Stripe token missing! Stop hacking mah forms!')
			elif flask.session['money'] and field == 'stripeEmail':
				flask.flash('Stripe email address invalid!')
			elif field not in ('csrf_token', 'recaptcha', 'stripeToken', 'stripeEmail'):
				for error in errors:
					for field, errors in error.items():
						if not csrf_failed and field == 'csrf_token':
							csrf_failed = True
							flask.flash('CSRF token missing! Stop hacking mah forms!')
						elif not id_failed and field == 'ticket_id':
							id_failed = True
							flask.flash('ID missing! Stop hacking mah forms!')
						elif not name_failed and field == 'name':
							name_failed = True
							flask.flash('Name is required!')
						elif not email_failed and field == 'contact_email':
							email_failed = True
							flask.flash('Email address is required, and must be a valid email address!')
						elif field not in ('csrf_token', 'ticket_id', 'name', 'contact_email'):
							flask.flash('%s: %s' % (field, ', '.join(errors)))
		return flask.redirect(flask.url_for('details'))

@app.route('/confirmation')
def confirmation():
	guests = [models.Guest.query.get(guest) for guest in flask.session['guests']]
	return flask.render_template('confirmation.html', guests=guests)

@app.route('/checkin')
def checkin():
	auth = flask.request.authorization
	if not auth or not ( auth.username == app.config['USERNAME'] and auth.password == app.config['PASSWORD'] ):
		return "Authorization required", 401, {"WWW-Authenticate": 'Basic realm="Login required"'}
	guests = models.Guest.query.all()
	guests_len = len(guests)
	forms.Guests.guests = fields.FieldList(fields.FormField(forms.Guest), min_entries=guests_len, max_entries=guests_len)
	form = forms.Guests()
	comboGuestsModelForm = zip(guests, form.guests)
	for modelGuest, formGuest in comboGuestsModelForm:
		formGuest.form.guest_id.data = modelGuest.id
		formGuest.form.checked_in.data = modelGuest.checked_in
		formGuest.form.name.data = modelGuest.name
		formGuest.form.contact_email.data = modelGuest.contact_email
		formGuest.form.company.data = modelGuest.company
		formGuest.form.website.data = modelGuest.website
		formGuest.form.twitter.data = modelGuest.twitter
		formGuest.form.callsign.data = modelGuest.callsign
		if modelGuest.ticket.kind == 'paid':
			formGuest.form.shirt_size.data = modelGuest.options['shirt_size']
		formGuest.form.amateur_exam.data = modelGuest.options['amateur_exam']
		formGuest.form.hackfest.data = modelGuest.options['hackfest']
		formGuest.form.email_me.data = modelGuest.options['email_me']
	return flask.render_template('checkin.html', form=form, comboGuestsModelForm=comboGuestsModelForm)

@app.route('/checkin', methods=['POST'])
def checkin_submit():
	auth = flask.request.authorization
	if not auth or not ( auth.username == app.config['USERNAME'] and auth.password == app.config['PASSWORD'] ):
		return "Authorization required", 401, {"WWW-Authenticate": 'Basic realm="Login required"'}
	guests = models.Guest.query.all()
	guests_dict = {guest.id: guest for guest in guests}
	form = forms.Guests()
	if form.validate_on_submit():
		for guestFormField in form.guests:
			guestForm = guestFormField.form
			guest = guests_dict[guestForm.guest_id.data]
			if guestForm.edited.data:
				guest.checked_in = guestForm.checked_in.data
				guest.name = guestForm.name.data
				guest.contact_email = guestForm.contact_email.data
				guest.company = guestForm.company.data
				guest.website = guestForm.website.data
				guest.twitter = guestForm.twitter.data
				guest.callsign = guestForm.callsign.data
				options = {}
				if guest.ticket.kind == 'paid':
					options['shirt_size'] = guestForm.shirt_size.data
				options['amateur_exam'] = guestForm.amateur_exam.data
				options['hackfest'] = guestForm.hackfest.data
				options['email_me'] = guestForm.email_me.data
				guest.options = {}
				guest.options.update(options)
			elif not guest.checked_in:
				guest.checked_in = guestForm.checked_in.data
			db.session.add(guest)
		db.session.commit()
	else:
		flask.flash(repr(form.errors))
	return flask.redirect(flask.url_for('checkin'))
