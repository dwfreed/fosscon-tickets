from . import db, utils

class Ticket(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String, unique=True)
	kind = db.Column(db.String)
	description = db.Column(db.String)
	cost = db.Column(db.Integer)

	def __init__(self, name, kind, description, cost):
		self.name = name
		self.kind = kind
		self.description = description
		self.cost = cost

class Guest(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String)
	contact_email = db.Column(db.String)
	company = db.Column(db.String)
	website = db.Column(db.String)
	twitter = db.Column(db.String)
	callsign = db.Column(db.String)
	ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
	ticket = db.relationship('Ticket')
	options = db.Column(utils.JSONEncodedDict)
	stripeCustomer = db.Column(db.String)
	checked_in = db.Column(db.Boolean)

	def __init__(self, name, contact_email, company, website, twitter, callsign, ticket_id, options, stripeCustomer=None):
		self.name = name
		self.contact_email = contact_email
		self.company = company
		self.website = website
		self.twitter = twitter
		self.callsign = callsign
		self.ticket = Ticket.query.get(ticket_id)
		self.options = {}
		self.options.update(options)
		self.stripeCustomer = stripeCustomer
		self.checked_in = False
